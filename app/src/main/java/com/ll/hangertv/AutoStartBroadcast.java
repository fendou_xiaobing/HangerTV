package com.ll.hangertv;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AutoStartBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent autoStart = new Intent(context, SplashActivity.class);
        autoStart.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(autoStart);
    }
}

